#!make

## migrate database and seed 
build_database:
	cd server && npm run migrate && npm run seed

## run server application
run_server:
	cd server && npm start

## run client application
run_client:
	cd client && npm start 

## install all dependencies
install:
	cd client && npm install
	cd server && npm install

## run application
ci:
	cd server && npm run test
	cd client && npm run test -- --watchAll=false