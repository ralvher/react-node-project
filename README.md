#How to run the application in local

Ensure you are using node 16.

First of all we need to install all required dependecies, so we need to execute:
```
 make install
```

After that, we have to migrate and see the database used in the server:

```
make build_database
``` 

When all this commands are executed finally to run the application with the following commands, and go to [localhost:3000](http://localhost:3000/). Use raquel@email.com to find orders
```
make run_server
make run_client
```

If you want to lunch the tests, you only have to use:
```
make ci
```