import React from "react";
import { Redirect, Route, Switch } from "react-router-dom";

import Box from "./UI/component/Box";
import OrdersPage from "./UI/pages/Orders";
import OrderDetails from "./UI/pages/OrderDetails";
import { AppRoutes } from "./Infrastructure/routes/AppRoutes";

const App = (): JSX.Element => {
  return (
    <Box
      width="100vw"
      height="100vh"
      justifyContent="center"
      alignItems="center"
      padding="0"
    >
      <Switch>
        <Route
          exact
          path={AppRoutes.orders.path}
          render={({ history }) => (
            <OrdersPage
              onGoToOrder={(number: string) =>
                history.push(AppRoutes.orderDetails.getUrl({ number }))
              }
            />
          )}
        />
        <Route
          exact
          path={AppRoutes.orderDetails.path}
          render={({ match }) => <OrderDetails number={match.params.number} />}
        />
        <Redirect
          to={{
            ...location,
            pathname: "/orders",
          }}
        />
      </Switch>
    </Box>
  );
};

export default App;
