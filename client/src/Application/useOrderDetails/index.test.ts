import { renderHook } from "@testing-library/react-hooks";

import useOrdersDetails from ".";

const expectedValue = {
  articles: [
    {
      image: "imageUrl",
      name: "article",
      number: "articleNumber",
      quantity: 1,
    },
  ],
  checkpoint: {
    details: "statusDetails",
    status: "Delivered",
  },
  city: "city",
  number: "orderNumber",
  trackingNumber: "trackingNumber",
  street: "street",
  zipCode: 12345,
};

jest.mock("../../Domain/services/OrderService", () => ({
  findAllByEmail: jest.fn(),
  findOneByNumber: () => ({ details: expectedValue }),
}));

describe("useOrdersDetails", () => {
  it("should call to OrderService to retrieve the data", async () => {
    const { result, waitForNextUpdate } = renderHook(() =>
      useOrdersDetails(expectedValue.number),
    );

    await waitForNextUpdate();

    expect(result.current).toEqual({
      data: expectedValue,
      isIdle: false,
      isError: false,
      isLoading: false,
    });
  });
});
