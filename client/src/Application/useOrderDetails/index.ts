import { useCallback, useEffect, useState } from "react";

import OrderService from "../../Domain/services/OrderService";
import { OrderDetails } from "../../Domain/types/OrderDetails";

const useOrderDetails = (number: string) => {
  const [state, setState] = useState<{
    isIdle: boolean;
    isLoading: boolean;
    isError: boolean;
    data?: OrderDetails;
  }>({ isIdle: true, isError: false, isLoading: false });

  const fetchData = useCallback(
    async (number: string) => {
      setState((currentState) => ({
        ...currentState,
        isLoading: true,
        isIdle: false,
      }));
      const result = await OrderService.findOneByNumber(number);
      setState((currentState) => ({
        ...currentState,
        data: result.details,
        isError: !!result.error,
        isLoading: false,
      }));
    },
    [setState],
  );

  useEffect(() => {
    if (!number) return;
    fetchData(number);
  }, [number]);

  return {
    data: state?.data,
    isIdle: state.isIdle,
    isError: state?.isError,
    isLoading: state?.isLoading,
  };
};

export default useOrderDetails;
