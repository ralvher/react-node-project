import { renderHook } from "@testing-library/react-hooks";

import useOrders from ".";
import { act } from "@testing-library/react";

const expectedValue = [
  {
    city: "city",
    checkpointStatus: "Delivered",
    number: "orderNumber",
    street: "street",
    trackingNumber: "trackingNumber",
    zipCode: 12345,
  },
];

jest.mock("../../Domain/services/OrderService", () => ({
  findAllByEmail: () => ({ orders: expectedValue }),
  findOneByNumber: jest.fn(),
}));

describe("useOrdersDetails", () => {
  it("should call to OrderService to retrieve the data", async () => {
    const { result, waitForNextUpdate } = renderHook(() => useOrders());

    expect(result.current).toEqual({
      data: [],
      isIdle: true,
      isError: false,
      isLoading: false,
      fetchData: expect.any(Function),
    });

    act(() => {
      result.current.fetchData("email@email.com");
    });

    await waitForNextUpdate();

    expect(result.current).toEqual({
      data: expectedValue,
      isIdle: false,
      isError: false,
      isLoading: false,
      fetchData: expect.any(Function),
    });
  });
});
