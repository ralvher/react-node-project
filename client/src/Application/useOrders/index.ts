import { useCallback, useState } from "react";

import { Order } from "../../Domain/types/Order";
import OrderService from "../../Domain/services/OrderService";

const useOrders = () => {
  const [state, setState] = useState<{
    isIdle: boolean;
    isLoading: boolean;
    isError: boolean;
    data: Order[];
  }>({ isIdle: true, isError: false, isLoading: false, data: [] });

  const fetchData = useCallback(
    async (email: string) => {
      setState((currentState) => ({
        ...currentState,
        isLoading: true,
        isIdle: false,
      }));
      const result = await OrderService.findAllByEmail(email);
      setState((currentState) => ({
        ...currentState,
        data: result.orders || [],
        isError: !!result.error,
        isLoading: false,
      }));
    },
    [setState],
  );

  return {
    data: state?.data,
    isIdle: state.isIdle,
    isError: state?.isError,
    isLoading: state?.isLoading,
    fetchData,
  };
};

export default useOrders;
