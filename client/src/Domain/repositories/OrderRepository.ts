import { Order } from "../types/Order";
import { OrderDetails } from "../types/OrderDetails";

interface ErrorResponse {
  error?: string;
}
interface GetInfoByNumberResponse extends ErrorResponse {
  details?: OrderDetails;
}

interface GetByEmailResponse extends ErrorResponse {
  orders?: Order[];
}

export interface OrderRepository {
  getByEmail: (email: string) => Promise<GetByEmailResponse>;
  getInfoByNumber: (number: string) => Promise<GetInfoByNumberResponse>;
}
