import OrderRepository from "../../../Infrastructure/repositories/OrderRepository";

const EMAIL_REGEX =
  /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/;

export enum OrderServiceErrors {
  INVALID_ORDER_EMAIL = "invalid-order-email",
  INVALID_ORDER_NUMBER = "invalid-order-number",
}

const OrderService = {
  findAllByEmail: (email: string) => {
    if (!email || !email.match(EMAIL_REGEX))
      throw Error(OrderServiceErrors.INVALID_ORDER_EMAIL);
    return OrderRepository.getByEmail(email);
  },
  findOneByNumber: (number: string) => {
    if (!number) throw Error(OrderServiceErrors.INVALID_ORDER_NUMBER);
    return OrderRepository.getInfoByNumber(number);
  },
};

export default OrderService;
