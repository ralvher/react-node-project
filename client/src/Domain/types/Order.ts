export interface Order {
  checkpointStatus: string;
  city: string;
  number: string;
  trackingNumber: string;
  street: string;
  zipCode: number;
}
