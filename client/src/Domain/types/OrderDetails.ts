interface OrderDetailsArticle {
  image: string;
  name: string;
  number: string;
  quantity: number;
}

interface OrderDetailsCheckpoint {
  details: string;
  status: string;
}

export interface OrderDetails {
  articles: OrderDetailsArticle[];
  checkpoint: OrderDetailsCheckpoint;
  city: string;
  number: string;
  trackingNumber: string;
  street: string;
  zipCode: number;
}
