import { OrderBuilder, OrdersBuilder } from ".";

const dataToBuild = {
  city: "city",
  checkpointStatus: "Delivered",
  number: "orderNumber",
  street: "street",
  trackingNumber: "trackingNumber",
  zipCode: 12345,
};

describe("OrderBuilder", () => {
  it("should build correct object", () => {
    const result = OrderBuilder({
      data: dataToBuild,
    });

    expect(result).toEqual({
      city: dataToBuild.city,
      checkpointStatus: dataToBuild.checkpointStatus,
      number: dataToBuild.number,
      street: dataToBuild.street,
      trackingNumber: dataToBuild.trackingNumber,
      zipCode: dataToBuild.zipCode,
    });
  });
});

describe("OrdersBuilder", () => {
  it("should build correct object", () => {
    const result = OrdersBuilder({
      data: [dataToBuild],
    });

    expect(result).toEqual([
      {
        city: dataToBuild.city,
        checkpointStatus: dataToBuild.checkpointStatus,
        number: dataToBuild.number,
        street: dataToBuild.street,
        trackingNumber: dataToBuild.trackingNumber,
        zipCode: dataToBuild.zipCode,
      },
    ]);
  });
});
