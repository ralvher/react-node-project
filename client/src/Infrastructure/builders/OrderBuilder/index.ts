import { Order } from "../../../Domain/types/Order";

interface OrderDTO {
  city: string;
  checkpointStatus: string;
  number: string;
  street: string;
  trackingNumber: string;
  zipCode: number;
}

const OrderBuilder = ({
  data: { city, checkpointStatus, number, street, trackingNumber, zipCode },
}: {
  data: OrderDTO;
}): Order => ({
  city,
  checkpointStatus,
  number,
  street,
  trackingNumber,
  zipCode,
});

const OrdersBuilder = ({ data }: { data: OrderDTO[] }): Order[] => {
  return data.map((order) => OrderBuilder({ data: order }));
};

export { OrderBuilder, OrdersBuilder };
