import { OrderDetailsBuilder } from ".";

const dataToBuild = {
  articles: [
    {
      image: "imageUrl",
      name: "article",
      number: "articleNumber",
      quantity: 1,
    },
  ],
  checkpoint: {
    details: "statusDetails",
    status: "Delivered",
  },
  city: "city",
  number: "orderNumber",
  trackingNumber: "trackingNumber",
  street: "street",
  zipCode: 12345,
};

describe("OrderDetailsBuilder", () => {
  it("should build correct object", () => {
    const result = OrderDetailsBuilder({
      data: dataToBuild,
    });

    expect(result).toEqual({
      articles: dataToBuild.articles,
      checkpoint: dataToBuild.checkpoint,
      city: dataToBuild.city,
      number: dataToBuild.number,
      trackingNumber: dataToBuild.trackingNumber,
      street: dataToBuild.street,
      zipCode: dataToBuild.zipCode,
    });
  });
});
