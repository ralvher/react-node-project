import { OrderDetails } from "../../../Domain/types/OrderDetails";

interface OrderCheckpointDTO {
  details: string;
  status: string;
}

interface OrderArticleDTO {
  image: string;
  name: string;
  number: string;
  quantity: number;
}

interface OrderDetailsDTO {
  articles: OrderArticleDTO[];
  checkpoint: OrderCheckpointDTO;
  city: string;
  number: string;
  street: string;
  trackingNumber: string;
  zipCode: number;
}

const OrderDetailsBuilder = ({
  data: { city, number, street, trackingNumber, zipCode, articles, checkpoint },
}: {
  data: OrderDetailsDTO;
}): OrderDetails => ({
  city,
  number,
  street,
  trackingNumber,
  zipCode,
  articles,
  checkpoint,
});

export { OrderDetailsBuilder };
