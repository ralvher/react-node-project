import "jest";
import fetchMock from "jest-fetch-mock";

import OrderRepository from "./OrderRepository";
import { OrdersBuilder } from "../builders/OrderBuilder";
import { OrderDetailsBuilder } from "../builders/OrderDetailsBuilder";

const payloadOrders = [
  {
    checkpointStatus: "Delivered",
    city: "city",
    number: "orderNumber",
    trackingNumber: "trackingNumber",
    street: "street",
    zipCode: 12345,
  },
];

const expectedOrders = OrdersBuilder({ data: payloadOrders });

const payloadOrderDetails = {
  articles: [
    {
      image: "imageUrl",
      name: "article",
      number: "articleNumber",
      quantity: 1,
    },
  ],
  checkpoint: {
    details: "statusDetails",
    status: "Delivered",
  },
  city: "city",
  number: "orderNumber",
  trackingNumber: "trackingNumber",
  street: "street",
  zipCode: 12345,
};

const expectedOrderDetails = OrderDetailsBuilder({ data: payloadOrderDetails });

beforeEach(() => {
  fetchMock.resetMocks();
});

describe("getByEmail", () => {
  it("should work correctly and retrieve data", async () => {
    fetchMock.mockResponse(JSON.stringify(payloadOrders));

    const email = "email@email.com";
    const { orders, error } = await OrderRepository.getByEmail(email);

    expect(error).toBeUndefined();
    expect(orders).toEqual(expectedOrders);
  });

  it("should work and retrive an 422 status", async () => {
    fetchMock.mockResponse(
      JSON.stringify({
        message: "invalid email",
      }),
      { status: 422 },
    );

    const email = "email@email.com";
    const { orders, error } = await OrderRepository.getByEmail(email);

    expect(error).toEqual("invalid email");
    expect(orders).toBeUndefined();
  });

  it("should throw an error if call fails", async () => {
    fetchMock.mockReject(new Error("Timeout"));

    const email = "email@email.com";

    await expect(OrderRepository.getByEmail(email)).rejects.toThrow("Timeout");
  });
});

describe("getInfoByNumber", () => {
  it("should work correctly and retrieve data", async () => {
    fetchMock.mockResponse(JSON.stringify(payloadOrderDetails));

    const number = "orderNumber";
    const { details, error } = await OrderRepository.getInfoByNumber(number);

    expect(error).toBeUndefined();
    expect(details).toEqual(expectedOrderDetails);
  });

  it("should throw an error if call fails", async () => {
    fetchMock.mockReject(new Error("Timeout"));

    const number = "orderNumber";

    await expect(OrderRepository.getInfoByNumber(number)).rejects.toThrow(
      "Timeout",
    );
  });
});
