import { OrderRepository } from "../../Domain/repositories/OrderRepository";
import { Order } from "../../Domain/types/Order";
import { OrderDetails } from "../../Domain/types/OrderDetails";

const API_URL = "http://localhost";

const ApiOrderRepository: OrderRepository = {
  getByEmail: async (email: string) => {
    const params = new URLSearchParams({
      email,
    });
    const response = await fetch(`${API_URL}/orders?` + params, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await response.json();

    if (response.status === 200) {
      return { orders: data as Order[] };
    }

    return { error: data.message || "unknown error" };
  },
  getInfoByNumber: async (number: string) => {
    const response = await fetch(`${API_URL}/orders/${number}`, {
      method: "GET",
      headers: {
        "Content-Type": "application/json",
      },
    });

    const data = await response.json();

    if (response.status === 200) {
      return { details: data as OrderDetails };
    }

    return { error: data.message || "unknown error" };
  },
};

export default ApiOrderRepository;
