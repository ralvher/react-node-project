const APP_BASE_PATH = "/orders";

const AppRoutes = {
  orders: {
    path: APP_BASE_PATH,
  },
  orderDetails: {
    path: `${APP_BASE_PATH}/:number`,
    getUrl: ({ number }: { number: string }): string =>
      `${APP_BASE_PATH}/${number}`,
  },
};

export { AppRoutes };
