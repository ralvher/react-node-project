import React, { CSSProperties, MouseEventHandler, ReactNode } from "react";

interface BoxProps {
  children: ReactNode;
  padding?: number | string;
  flexDirection?: CSSProperties["flexDirection"];
  justifyContent?: CSSProperties["justifyContent"];
  alignItems?: CSSProperties["alignItems"];
  maxWidth?: number | string;
  width?: number | string;
  maxHeight?: number | string;
  height?: number | string;
  shadow?: boolean;
  onClick?: MouseEventHandler;
}
const Box = ({
  children,
  padding = 24,
  flexDirection = "row",
  alignItems = "stretch",
  justifyContent = "flex-start",
  maxWidth,
  width,
  height,
  maxHeight,
  shadow = true,
  onClick,
}: BoxProps): JSX.Element => {
  return (
    <div
      style={{
        borderRadius: 10,
        boxShadow: shadow ? "0 0 5px #cfcfcf" : "none",
        padding: Number(padding),
        display: "flex",
        flexDirection,
        justifyContent,
        alignItems,
        width,
        maxWidth,
        height,
        maxHeight,
        cursor: onClick ? "pointer" : "normal",
      }}
      onClick={onClick}
    >
      {children}
    </div>
  );
};

export default Box;
