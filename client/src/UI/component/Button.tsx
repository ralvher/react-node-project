import React, { ButtonHTMLAttributes } from "react";

import Text from "./Text";

interface ButtonProps extends ButtonHTMLAttributes<HTMLButtonElement> {
  width?: string | number;
}
const Button = ({
  children,
  onClick,
  width,
  disabled,
  ...rest
}: ButtonProps): JSX.Element => {
  return (
    <button
      disabled={disabled}
      style={{
        backgroundColor: disabled ? "grey" : "#000e41",
        borderRadius: 6,
        cursor: disabled ? "none" : "pointer",
        padding: "8px 16px",
        border: "none",
        fontFamily: "inherit",
        width,
        height: 34,
      }}
      onClick={onClick}
      {...rest}
    >
      <Text color="white" upperCase fontSize={14} bold>
        {children}
      </Text>
    </button>
  );
};

export default Button;
