import React, { CSSProperties, ReactNode } from "react";

import Text from "./Text";

interface FieldProps {
  children: ReactNode;
  name: string;
  required?: boolean;
  title?: string;
  error?: string;
  width?: CSSProperties["width"];
}
const Field = ({
  children,
  name,
  required,
  title,
  error,
  width = "100%",
}: FieldProps): JSX.Element => {
  return (
    <div style={{ width, position: "relative" }}>
      {title && (
        <label htmlFor={name} style={{ marginBottom: 4 }}>
          <Text>{title}</Text>
          {required && <Text bold> *</Text>}
        </label>
      )}
      {children}
      {error && (
        <Text
          fontSize="10"
          color="palevioletred"
          position="absolute"
          bottom={-18}
        >
          {error}
        </Text>
      )}
    </div>
  );
};

export default Field;
