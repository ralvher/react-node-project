import React, { InputHTMLAttributes } from "react";

interface InputProps extends InputHTMLAttributes<HTMLInputElement> {
  error?: boolean;
  width?: string | number;
}
const Input = ({ error, width, ...rest }: InputProps): JSX.Element => {
  return (
    <input
      style={{
        borderColor: error ? "palevioletred" : "transparent",
        borderWidth: 2,
        borderStyle: "solid",
        backgroundColor: "#f0f0f0",
        borderRadius: 6,
        padding: 6,
        color: "darkslategrey",
        fontFamily: "inherit",
        outline: "none",
        boxSizing: "border-box",
        width,
      }}
      {...rest}
    />
  );
};

export default Input;
