import React, { ReactNode } from "react";

type Space = "auto" | "0" | "4" | "8" | "16" | "24" | 0 | 4 | 8 | 16 | 24;

interface MarginProps {
  children: ReactNode;
  left?: Space;
  right?: Space;
  top?: Space;
  bottom?: Space;
}
const Margin = ({
  children,
  top = 0,
  right = 0,
  bottom = 0,
  left = 0,
}: MarginProps): JSX.Element => {
  return (
    <div
      style={{
        marginTop: Number(top),
        marginRight: Number(right),
        marginBottom: Number(bottom),
        marginLeft: Number(left),
      }}
    >
      {children}
    </div>
  );
};

export default Margin;
