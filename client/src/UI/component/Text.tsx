import React, { CSSProperties, ReactNode } from "react";

export type Color = "black" | "grey" | "palevioletred" | "white";
export type FontSize = "8" | "10" | "12" | "14" | "18" | 8 | 10 | 12 | 14 | 18;

interface TextProps {
  children: ReactNode;
  color?: Color;
  bold?: boolean;
  fontSize?: FontSize;
  upperCase?: boolean;
  position?: CSSProperties["position"];
  top?: CSSProperties["top"];
  bottom?: CSSProperties["bottom"];
  left?: CSSProperties["left"];
  right?: CSSProperties["right"];
}

const Text = ({
  children,
  color = "black",
  bold = false,
  fontSize = 14,
  upperCase = false,
  position,
  bottom,
  top,
  left,
  right,
}: TextProps): JSX.Element => {
  return (
    <div
      style={{
        color,
        fontWeight: bold ? "bold" : "normal",
        fontSize: Number(fontSize),
        textTransform: upperCase ? "uppercase" : "none",
        position,
        bottom,
        top,
        left,
        right,
      }}
    >
      {children}
    </div>
  );
};

export default Text;
