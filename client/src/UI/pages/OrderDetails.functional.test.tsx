import "jest";
import { render, screen } from "@testing-library/react";

import OrderDetailsPage from "./OrderDetails";
import { OrderDetails } from "../../Domain/types/OrderDetails";

const expectedOrderDetails = {
  articles: [
    {
      image: "imageUrl",
      name: "article",
      number: "articleNumber",
      quantity: 1,
    },
  ],
  checkpoint: {
    details: "statusDetails",
    status: "Delivered",
  },
  city: "city",
  number: "orderNumber",
  trackingNumber: "trackingNumber",
  street: "street",
  zipCode: 12345,
};

let mockIsError = false;
let mockData: OrderDetails | null = null;
let mockIsLoading = false;
jest.mock("../../Application/useOrderDetails", () => () => ({
  isIdle: false,
  isLoading: mockIsLoading,
  isError: mockIsError,
  data: mockData,
  fetchData: jest.fn(),
}));

afterEach(() => {
  mockIsError = false;
  mockData = null;
  mockIsLoading = false;
});

test("Display loading data", () => {
  mockIsLoading = true;

  render(<OrderDetailsPage number="orderNumber" />);

  const message = screen.getByText(/Loading data/);

  expect(message).toBeInTheDocument();
});

test("Display an error message when order is not found due to an error", () => {
  mockIsError = true;

  render(<OrderDetailsPage number="orderNumber" />);

  const message = screen.getByText(/Ops! Something went wrong/);

  expect(message).toBeInTheDocument();
});

test("Display an message when order is not found", () => {
  render(<OrderDetailsPage number="orderNumber" />);

  const message = screen.getByText(/Order not found/);

  expect(message).toBeInTheDocument();
});

test("Display order details", () => {
  mockData = expectedOrderDetails;

  render(<OrderDetailsPage number="orderNumber" />);

  const trackingNumber = screen.getByText("trackingNumber");
  const article = screen.getByText("articleNumber");

  expect(trackingNumber).toBeInTheDocument();
  expect(article).toBeInTheDocument();
});
