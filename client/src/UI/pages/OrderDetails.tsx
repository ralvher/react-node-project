import React from "react";

import Box from "../component/Box";
import Text from "../component/Text";
import Margin from "../component/Margin";
import useOrderDetails from "../../Application/useOrderDetails";

interface OrderDetailsProps {
  number?: string;
}

const OrderDetailsPage = ({ number }: OrderDetailsProps): JSX.Element => {
  const { data: details, isError, isLoading } = useOrderDetails(String(number));

  if (isLoading) {
    return (
      <Box flexDirection="column" maxWidth={350} width="100%">
        <Text>Loading data...</Text>
      </Box>
    );
  }

  if (isError) {
    return (
      <Box flexDirection="column" maxWidth={350} width="100%">
        <Text>Ops! Something went wrong and you order couldn't be find</Text>
      </Box>
    );
  }

  if (!details) {
    return (
      <Box flexDirection="column" maxWidth={350} width="100%">
        <Text>Order not found</Text>
      </Box>
    );
  }

  const { trackingNumber, articles, city, checkpoint, street, zipCode } =
    details;
  return (
    <Box flexDirection="column" maxWidth={350} width="100%">
      <Margin bottom="8">
        <Text color="grey" fontSize="10">
          Order Number
        </Text>
        <Text bold>{number}</Text>
      </Margin>
      <Margin bottom="16">
        <Text color="grey" fontSize="10">
          Delivery Address
        </Text>
        <Text bold>{street}</Text>
        <Text bold>
          {zipCode} {city}
        </Text>
      </Margin>
      <Margin bottom="16">
        <Box flexDirection="column" padding="8">
          <Margin bottom="8">
            <Text color="grey" fontSize="10">
              Tracking number
            </Text>
            <Text bold>{trackingNumber}</Text>
          </Margin>
          <Margin bottom="8">
            <Text color="grey" fontSize="10">
              Current Status
            </Text>
            <Text bold>{checkpoint.status}</Text>
            <Text fontSize="12">{checkpoint.details}</Text>
          </Margin>
        </Box>
      </Margin>

      {!!articles.length && (
        <Box flexDirection="column" padding="8">
          <Text color="grey" fontSize="10">
            Articles
          </Text>
          {articles.map(({ quantity, image, name, number }) => (
            <Box
              key={number}
              shadow={false}
              height="65"
              padding="0"
              alignItems="center"
            >
              <Text color="grey">x{quantity}</Text>
              <Margin right="16" left="16">
                <img width="30" src={image}></img>
              </Margin>
              <div>
                <Text>{name}</Text>
                <Text fontSize="10" color="grey">
                  {number}
                </Text>
              </div>
            </Box>
          ))}
        </Box>
      )}
    </Box>
  );
};

export default OrderDetailsPage;
