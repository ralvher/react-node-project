import "jest";
import {
  act,
  fireEvent,
  render,
  screen,
  waitFor,
} from "@testing-library/react";
import userEvent from "@testing-library/user-event";

import OrdersPage from "./Orders";
import { Order } from "../../Domain/types/Order";

const ordersResponse = [
  {
    checkpointStatus: "Delivered",
    city: "city",
    number: "orderNumber",
    trackingNumber: "trackingNumber",
    street: "street",
    zipCode: 12345,
  },
];

let mockIsIdle = true;
let mockData: Order[] = [];
jest.mock("../../Application/useOrders", () => () => ({
  isIdle: mockIsIdle,
  isLoading: false,
  isError: false,
  data: mockData,
  fetchData: jest.fn(),
}));

afterEach(() => {
  mockData = [];
  mockIsIdle = true;
});

test("Allows to introduce and email and submit", async () => {
  render(<OrdersPage onGoToOrder={jest.fn()} />);

  const input = screen.getByPlaceholderText("Enter an email");
  act(() => {
    fireEvent.change(input, { target: { value: "email@email.com" } });
  });

  mockIsIdle = false;

  const submit = screen.getByText("Search");
  userEvent.click(submit);

  const ordersMessage = await screen.findByText(
    "You don't have any orders yet.",
  );
  expect(ordersMessage).toBeInTheDocument();
});

test("Allows to click on an order", async () => {
  const onGoToOrderSpy = jest.fn();

  render(<OrdersPage onGoToOrder={onGoToOrderSpy} />);

  const input = screen.getByPlaceholderText("Enter an email");
  act(() => {
    fireEvent.change(input, { target: { value: "email@email.com" } });
  });

  mockData = ordersResponse;
  mockIsIdle = false;

  const submit = screen.getByText("Search");
  userEvent.click(submit);

  const orderNumber = await screen.findByText(ordersResponse[0].number);
  expect(orderNumber).toBeInTheDocument();

  userEvent.click(orderNumber);
  await waitFor(() =>
    expect(onGoToOrderSpy).toHaveBeenCalledWith(ordersResponse[0].number),
  );
});
