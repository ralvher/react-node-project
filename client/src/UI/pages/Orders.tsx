import React, { ChangeEvent, FormEvent, useState } from "react";

import Box from "../component/Box";
import Text from "../component/Text";
import Field from "../component/Field";
import Input from "../component/Input";
import Margin from "../component/Margin";
import Button from "../component/Button";
import useOrders from "../../Application/useOrders";

const EMAIL_REGEX =
  /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/;

interface OrdersState {
  email: string;
  error: { message: string };
}

interface OrdersProps {
  onGoToOrder: (number: string) => void;
}

const OrdersPage = ({ onGoToOrder }: OrdersProps) => {
  const [state, setState] = useState<OrdersState>({
    email: "",
    error: { message: "" },
  });
  const { email, error } = state;

  const {
    data: orders,
    isError,
    isIdle,
    isLoading,
    fetchData: fetchOrders,
  } = useOrders();

  const handleEmailChange = (e: ChangeEvent<HTMLInputElement>) => {
    const change = e.target.value;

    setState((currentState) => ({
      ...currentState,
      error: { message: change?.match(EMAIL_REGEX) ? "" : "Invalid email" },
      email: change,
    }));
  };

  const handleSubmitForm = async (e: FormEvent) => {
    e.preventDefault();

    if (!error.message) {
      fetchOrders(email);
      setState((currentState) => ({
        ...currentState,
        error: { message: "" },
      }));
    }
  };

  return (
    <Box flexDirection="column" maxWidth={350} width="100%">
      <Margin bottom="16">
        <Text fontSize="18">
          Please enter your email address to see your recent orders
        </Text>
      </Margin>
      <Margin bottom={16}>
        <form onSubmit={handleSubmitForm}>
          <Box
            flexDirection="row"
            alignItems="end"
            justifyContent="space-between"
          >
            <Field name="email" title="Email" error={error.message}>
              <Input
                id="email"
                onChange={handleEmailChange}
                value={email}
                type="email"
                error={!!error.message}
                width="100%"
                placeholder="Enter an email"
              />
            </Field>

            <Margin left={8}>
              <Button
                type="submit"
                disabled={isLoading || !!state.error.message || !state.email}
              >
                Search
              </Button>
            </Margin>
          </Box>
        </form>
      </Margin>
      {isLoading && <Text>Loading data ...</Text>}
      {orders.map(({ checkpointStatus, city, number, street, zipCode }) => (
        <Margin key={number} bottom="16">
          <Box
            justifyContent="space-between"
            onClick={() => onGoToOrder(number)}
          >
            <div>
              <Margin bottom="8">
                <Text color="grey" fontSize="10">
                  Order Number
                </Text>
                <Text bold>{number}</Text>
              </Margin>
              <div>
                <Text color="grey" fontSize="10">
                  Delivery Address
                </Text>
                <Text bold>{street}</Text>
                <Text bold>
                  {zipCode} {city}
                </Text>
              </div>
            </div>
            <div>
              <div>
                <Text color="grey" fontSize="10">
                  Current Status
                </Text>
                <Text bold>{checkpointStatus}</Text>
              </div>
            </div>
          </Box>
        </Margin>
      ))}

      {!isLoading && !isIdle && !orders.length && (
        <Margin top={16}>
          <Text>You don't have any orders yet.</Text>
        </Margin>
      )}

      {!isIdle && isError && (
        <Margin top={16}>
          <Text>Opps... Something went wrong! </Text>
        </Margin>
      )}
    </Box>
  );
};

export default OrdersPage;
