const path = require("path");

module.exports = {
  transform: {
    "^.+\\.(ts|tsx)$": "ts-jest",
  },
  testEnvironment: path.join("<rootDir>/test/config-test-environment.js"),
  modulePathIgnorePatterns: ["build"],
};
