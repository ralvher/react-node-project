export class NotFoundError extends Error {
  constructor(m = "not found") {
    super(m);
  }
}
