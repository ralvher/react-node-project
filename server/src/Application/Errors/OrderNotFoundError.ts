import { NotFoundError } from "./NotFoundError";

export class OrderNotFoundError extends NotFoundError {
  constructor(m = "order not found") {
    super(m);
  }
}
