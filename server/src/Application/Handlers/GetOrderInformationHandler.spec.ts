import "jest";
import { Order } from "../../Domain/Models/Order";
import { Checkpoint } from "../../Domain/Models/Checkpoint";
import { CheckpointRepository } from "../../Domain/Repositories/CheckpointRepository";
import { OrderRepository } from "../../Domain/Repositories/OrderRepository";
import { GetOrderInformationHandler } from "./GetOrderInformationHandler";
import { ArticleRepository } from "../../Domain/Repositories/ArticleRepository";
import { GetOrderInformationQuery } from "../Queries/GetOrderInformationQuery";
import { OrderNotFoundError } from "../Errors/OrderNotFoundError";
import { Article } from "../../Domain/Models/Article";

describe("Get Orders Information Handler", () => {
  it("should throw an error if order not exist", async () => {
    const orderNumber = "orderNumber";
    const query = GetOrderInformationQuery.create(orderNumber);

    const orderRepositoryMock: OrderRepository = {
      findByEmail: jest.fn().mockResolvedValue(null),
      getByNumber: jest.fn(),
    };

    const checkpointRepositoryMock: CheckpointRepository = {
      getLastByOrderNumber: jest.fn(),
    };

    const articleRepositoryMock: ArticleRepository = {
      findByOrderNumber: jest.fn(),
    };

    const handler = new GetOrderInformationHandler(
      orderRepositoryMock,
      checkpointRepositoryMock,
      articleRepositoryMock,
    );

    await expect(handler.handle(query)).rejects.toThrowError(
      OrderNotFoundError,
    );
    expect(orderRepositoryMock.getByNumber).toHaveBeenCalledWith(
      query.getOrderNumber(),
    );
    expect(
      checkpointRepositoryMock.getLastByOrderNumber,
    ).not.toHaveBeenCalled();
    expect(articleRepositoryMock.findByOrderNumber).not.toHaveBeenCalled();
  });

  it("should return an  array with orders related with email", async () => {
    const orderId = 1;
    const trackingNumber = "trackingNumber";
    const orderNumber = "orderNumber";
    const courier = "courier";
    const street = "street";
    const zipCode = 23244;
    const city = "city";
    const destinationCountry = "destinationCountry";
    const email = "email@email.com";

    const query = GetOrderInformationQuery.create(orderNumber);

    const order = Order.hydrate(
      orderId,
      orderNumber,
      trackingNumber,
      courier,
      street,
      zipCode,
      city,
      destinationCountry,
      email,
    );

    const checkpointId = 2;
    const location = "location";
    const timestap = new Date();
    const status = "status";
    const statusText = "statusText";
    const statusDetails = "statusDetails";

    const checkpoint = Checkpoint.hydrate(
      checkpointId,
      orderId,
      location,
      timestap,
      status,
      statusText,
      statusDetails,
    );

    const articleId = 3;
    const articleNumber = "articleNumber";
    const imageUrl = "imageUrl";
    const quantity = 2;
    const name = "name";

    const article = Article.hydrate(
      articleId,
      articleNumber,
      imageUrl,
      quantity,
      name,
    );

    const orderRepositoryMock: OrderRepository = {
      findByEmail: jest.fn(),
      getByNumber: jest.fn().mockResolvedValue(order),
    };

    const checkpointRepositoryMock: CheckpointRepository = {
      getLastByOrderNumber: jest.fn().mockResolvedValue(checkpoint),
    };
    const articleRepositoryMock: ArticleRepository = {
      findByOrderNumber: jest.fn().mockResolvedValue([article]),
    };

    const handler = new GetOrderInformationHandler(
      orderRepositoryMock,
      checkpointRepositoryMock,
      articleRepositoryMock,
    );

    const response = await handler.handle(query);

    expect(orderRepositoryMock.getByNumber).toHaveBeenCalledWith(
      query.getOrderNumber(),
    );
    expect(checkpointRepositoryMock.getLastByOrderNumber).toHaveBeenCalledWith(
      order.getNumber(),
    );

    expect(articleRepositoryMock.findByOrderNumber).toHaveBeenCalledWith(
      order.getNumber(),
    );

    expect(response.toJson()).toEqual({
      articles: [
        {
          image: "imageUrl",
          name: "name",
          number: "articleNumber",
          quantity: 2,
        },
      ],
      checkpoint: { details: "statusDetails", status: "statusText" },
      order: {
        city: "city",
        number: "orderNumber",
        street: "street",
        trackingNumber: "trackingNumber",
        zipCode: 23244,
      },
    });
  });
});
