import "reflect-metadata";
import { inject, injectable } from "inversify";
import { OrderRepository } from "../../Domain/Repositories/OrderRepository";
import { CheckpointRepository } from "../../Domain/Repositories/CheckpointRepository";
import { GetOrderInformationQuery } from "../Queries/GetOrderInformationQuery";
import { ArticleRepository } from "../../Domain/Repositories/ArticleRepository";
import { GetOrderInformationResponse } from "../Responses/GetOrderInformationResponse";
import { OrderNotFoundError } from "../Errors/OrderNotFoundError";

@injectable()
export class GetOrderInformationHandler {
  private orderRepository: OrderRepository;
  private checkpointRepository: CheckpointRepository;
  private articleRepository: ArticleRepository;

  public constructor(
    @inject("OrderRepository") orderRepository: OrderRepository,
    @inject("CheckpointRepository") checkpointRepository: CheckpointRepository,
    @inject("ArticleRepository") articleRepository: ArticleRepository,
  ) {
    this.orderRepository = orderRepository;
    this.checkpointRepository = checkpointRepository;
    this.articleRepository = articleRepository;
  }

  public async handle(
    query: GetOrderInformationQuery,
  ): Promise<GetOrderInformationResponse> {
    const orderNumber = query.getOrderNumber();

    const order = await this.orderRepository.getByNumber(orderNumber);

    if (!order) {
      throw new OrderNotFoundError();
    }

    const checkpoint = await this.checkpointRepository.getLastByOrderNumber(
      orderNumber,
    );

    const articles = await this.articleRepository.findByOrderNumber(
      orderNumber,
    );

    return GetOrderInformationResponse.create(order, checkpoint, articles);
  }
}
