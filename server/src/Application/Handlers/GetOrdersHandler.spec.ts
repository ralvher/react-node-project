import "jest";
import { Order } from "../../Domain/Models/Order";
import { Checkpoint } from "../../Domain/Models/Checkpoint";
import { CheckpointRepository } from "../../Domain/Repositories/CheckpointRepository";
import { OrderRepository } from "../../Domain/Repositories/OrderRepository";
import { GetOrdersQuery } from "../Queries/GetOrdersQuery";
import { GetOrdersHandler } from "./GetOrdersHandler";

describe("Get Orders Handler", () => {
  it("should return an empty array if not orders related with email", async () => {
    const email = "email@email.com";
    const query = GetOrdersQuery.create(email);

    const orderRepositoryMock: OrderRepository = {
      findByEmail: jest.fn().mockResolvedValue([]),
      getByNumber: jest.fn(),
    };

    const checkpointRepositoryMock: CheckpointRepository = {
      getLastByOrderNumber: jest.fn(),
    };

    const handler = new GetOrdersHandler(
      orderRepositoryMock,
      checkpointRepositoryMock,
    );

    const response = await handler.handle(query);

    expect(orderRepositoryMock.findByEmail).toHaveBeenCalledWith(
      query.getEmail(),
    );
    expect(
      checkpointRepositoryMock.getLastByOrderNumber,
    ).not.toHaveBeenCalled();
    expect(response.toJson().orders).toEqual([]);
  });

  it("should return an  array with orders related with email", async () => {
    const orderId = 1;
    const trackingNumber = "trackingNumber";
    const orderNumber = "orderNumber";
    const courier = "courier";
    const street = "street";
    const zipCode = 23244;
    const city = "city";
    const destinationCountry = "destinationCountry";
    const email = "email@email.com";

    const query = GetOrdersQuery.create(email);

    const order = Order.hydrate(
      orderId,
      orderNumber,
      trackingNumber,
      courier,
      street,
      zipCode,
      city,
      destinationCountry,
      email,
    );

    const orders: Order[] = [order];

    const checkpointId = 2;
    const location = "location";
    const timestap = new Date();
    const status = "status";
    const statusText = "statusText";
    const statusDetails = "statusDetails";

    const checkpoint = Checkpoint.hydrate(
      checkpointId,
      orderId,
      location,
      timestap,
      status,
      statusText,
      statusDetails,
    );

    const orderRepositoryMock: OrderRepository = {
      findByEmail: jest.fn().mockResolvedValue(orders),
      getByNumber: jest.fn(),
    };

    const checkpointRepositoryMock: CheckpointRepository = {
      getLastByOrderNumber: jest.fn().mockResolvedValue(checkpoint),
    };

    const handler = new GetOrdersHandler(
      orderRepositoryMock,
      checkpointRepositoryMock,
    );

    const response = await handler.handle(query);

    expect(orderRepositoryMock.findByEmail).toHaveBeenCalledWith(
      query.getEmail(),
    );
    expect(checkpointRepositoryMock.getLastByOrderNumber).toHaveBeenCalledWith(
      order.getNumber(),
    );
    expect(response.toJson().orders).toEqual([
      {
        checkpoint: {
          status: "statusText",
        },
        order: {
          city: "city",
          number: "orderNumber",
          trackingNumber: "trackingNumber",
          street: "street",
          zipCode: 23244,
        },
      },
    ]);
  });
});
