import "reflect-metadata";
import { inject, injectable } from "inversify";
import { OrderRepository } from "../../Domain/Repositories/OrderRepository";
import { CheckpointRepository } from "../../Domain/Repositories/CheckpointRepository";
import { GetOrdersQuery } from "../Queries/GetOrdersQuery";
import { GetOrdersResponse } from "../Responses/GetOrdersResponse";
import { GetOrderResponse } from "../Responses/GetOrderResponse";

@injectable()
export class GetOrdersHandler {
  private orderRepository: OrderRepository;
  private checkpointRepository: CheckpointRepository;

  public constructor(
    @inject("OrderRepository") orderRepository: OrderRepository,
    @inject("CheckpointRepository") checkpointRepository: CheckpointRepository,
  ) {
    this.orderRepository = orderRepository;
    this.checkpointRepository = checkpointRepository;
  }

  public async handle(query: GetOrdersQuery): Promise<GetOrdersResponse> {
    const orders = await this.orderRepository.findByEmail(query.getEmail());

    const response: GetOrderResponse[] = [];

    for await (const order of orders) {
      const lastCheckpoint = await this.checkpointRepository.getLastByOrderNumber(
        order.getNumber(),
      );
      response.push(GetOrderResponse.create(order, lastCheckpoint));
    }

    return GetOrdersResponse.create(response);
  }
}
