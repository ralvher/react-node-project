import "jest";
import { GetOrderInformationQuery } from "./GetOrderInformationQuery";

describe("Get Orders Information Query", () => {
  it("should be created successfully", () => {
    const number = "order-number";
    const response = GetOrderInformationQuery.create(number);
    expect(response.getOrderNumber().getValue()).toEqual(number);
  });
});
