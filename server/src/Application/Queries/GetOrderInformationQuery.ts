import { OrderNumber } from "../../Domain/ValueObjects";

export class GetOrderInformationQuery {
  private orderNumber: OrderNumber;

  private constructor(email: string) {
    this.orderNumber = OrderNumber.create(email);
  }

  public static create(email: string): GetOrderInformationQuery {
    return new this(email);
  }

  getOrderNumber(): OrderNumber {
    return this.orderNumber;
  }
}
