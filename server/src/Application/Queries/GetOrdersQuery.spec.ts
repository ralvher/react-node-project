import "jest";
import { GetOrdersQuery } from "./GetOrdersQuery";

describe("Get Orders Query", () => {
  it("should be created successfully", () => {
    const email = "email@email.com";
    const response = GetOrdersQuery.create(email);
    expect(response.getEmail().getValue()).toEqual(email);
  });
});
