import { Email } from "../../Domain/ValueObjects";

export class GetOrdersQuery {
  private email: Email;

  private constructor(email: string) {
    this.email = Email.create(email);
  }

  public static create(email: string): GetOrdersQuery {
    return new this(email);
  }

  getEmail(): Email {
    return this.email;
  }
}
