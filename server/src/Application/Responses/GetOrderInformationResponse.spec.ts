import "jest";
import { Article } from "../../Domain/Models/Article";

import { Checkpoint } from "../../Domain/Models/Checkpoint";
import { Order } from "../../Domain/Models/Order";
import { GetOrderInformationResponse } from "./GetOrderInformationResponse";

describe("Get Order Information Response", () => {
  it("should be created successfully", () => {
    const orderId = 1;
    const trackingNumber = "trackingNumber";
    const orderNumber = "orderNumber";
    const courier = "courier";
    const street = "street";
    const zipCode = 1212;
    const city = "city";
    const destinationCountry = "";
    const email = "email@email.com";

    const order = Order.hydrate(
      orderId,
      orderNumber,
      trackingNumber,
      courier,
      street,
      zipCode,
      city,
      destinationCountry,
      email,
    );

    const checkpointId = 2;
    const statusText = "statusText";
    const statusDetails = "statusDetails";

    const checkpoint = Checkpoint.hydrate(
      checkpointId,
      orderId,
      null,
      null,
      null,
      statusText,
      statusDetails,
    );

    const articleId = 3;
    const articleNumber = "articleNumber";
    const imageUrl = "imageUrl";
    const articleQuantity = 2;
    const articleName = "name";
    const article = Article.hydrate(
      articleId,
      articleNumber,
      imageUrl,
      articleQuantity,
      articleName,
    );
    const articles = [article];

    const response = GetOrderInformationResponse.create(
      order,
      checkpoint,
      articles,
    );

    const expected = {
      order: {
        city,
        number: orderNumber,
        trackingNumber,
        street,
        zipCode,
      },
      checkpoint: {
        details: statusDetails,
        status: statusText,
      },
      articles: [
        {
          image: imageUrl,
          name: articleName,
          number: articleNumber,
          quantity: articleQuantity,
        },
      ],
    };

    expect(response.toJson()).toEqual(expected);
  });
});
