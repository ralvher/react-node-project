import { Article } from "../../Domain/Models/Article";
import { Checkpoint } from "../../Domain/Models/Checkpoint";
import { Order } from "../../Domain/Models/Order";

interface OrderResponse {
  number: string;
  trackingNumber: string;
  street: string;
  zipCode: number;
  city: string;
}

interface CheckpointResponse {
  status: string;
  details: string;
}

interface ArticlesResponse {
  name: string;
  image: string;
  quantity: number;
  number: string;
}

export interface GetOrderInformationResponseJson {
  order: OrderResponse;
  checkpoint: CheckpointResponse;
  articles: ArticlesResponse[];
}

export class GetOrderInformationResponse {
  private order: Order;
  private checkpoint: Checkpoint | null;
  private articles: Article[];

  private constructor(
    order: Order,
    checkpoint: Checkpoint | null,
    articles: Article[],
  ) {
    this.order = order;
    this.checkpoint = checkpoint;
    this.articles = articles;
  }

  public static create(
    order: Order,
    checkpoint: Checkpoint | null,
    articles: Article[],
  ): GetOrderInformationResponse {
    return new this(order, checkpoint, articles);
  }

  public toJson(): GetOrderInformationResponseJson {
    return {
      order: {
        number: this.order.getNumber().getValue(),
        trackingNumber: this.order.getTrackingNumber().getValue(),
        street: this.order.getStreet().getValue(),
        zipCode: this.order.getZipCode().getValue(),
        city: this.order.getCity().getValue(),
      },
      checkpoint: {
        status: this.checkpoint?.getStatusText().getValue() || "",
        details: this.checkpoint?.getStatusDetails().getValue() || "",
      },
      articles: this.articles.map((article) => ({
        name: article.getName().getValue(),
        image: article.getImageUrl().getValue(),
        quantity: article.getQuantity().getValue(),
        number: article.getNumber().getValue(),
      })),
    };
  }
}
