import "jest";

import { Checkpoint } from "../../Domain/Models/Checkpoint";
import { Order } from "../../Domain/Models/Order";
import { GetOrderResponse } from "./GetOrderResponse";

describe("Get Order response Response", () => {
  it("should be created successfully", () => {
    const orderId = 1;
    const number = "orderNumber";
    const trackingNumber = "trackingNumber";
    const courier = "courier";
    const street = "street";
    const zipCode = 1212;
    const city = "city";
    const destinationCountry = "";
    const email = "email@email.com";

    const tracking = Order.hydrate(
      orderId,
      number,
      trackingNumber,
      courier,
      street,
      zipCode,
      city,
      destinationCountry,
      email,
    );

    const checkpointId = 2;
    const statusText = "statusText";

    const checkpoint = Checkpoint.hydrate(
      checkpointId,
      orderId,
      null,
      null,
      null,
      statusText,
      null,
    );

    const response = GetOrderResponse.create(tracking, checkpoint);

    const expected = {
      order: {
        city: "city",
        trackingNumber: "trackingNumber",
        number: "orderNumber",
        street: "street",
        zipCode: 1212,
      },
      checkpoint: {
        status: statusText,
      },
    };

    expect(response.toJson()).toEqual(expected);
  });
});
