import { Checkpoint } from "../../Domain/Models/Checkpoint";
import { Order } from "../../Domain/Models/Order";

interface OrderResponse {
  number: string;
  trackingNumber: string;
  street: string;
  zipCode: number;
  city: string;
}

interface CheckpointResponse {
  status: string;
}
export interface GetOrderResponseJson {
  order: OrderResponse;
  checkpoint: CheckpointResponse;
}

export class GetOrderResponse {
  private order: Order;
  private checkpoint: Checkpoint | null;

  private constructor(order: Order, checkpoint: Checkpoint | null) {
    this.order = order;
    this.checkpoint = checkpoint;
  }

  public static create(
    order: Order,
    checkpoint: Checkpoint | null,
  ): GetOrderResponse {
    return new this(order, checkpoint);
  }

  public toJson(): GetOrderResponseJson {
    return {
      order: {
        number: this.order.getNumber().getValue(),
        trackingNumber: this.order.getTrackingNumber().getValue(),
        street: this.order.getStreet().getValue(),
        zipCode: this.order.getZipCode().getValue(),
        city: this.order.getCity().getValue(),
      },
      checkpoint: {
        status: this.checkpoint?.getStatusText().getValue() || "",
      },
    };
  }
}
