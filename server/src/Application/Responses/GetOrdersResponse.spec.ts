import "jest";

import { Checkpoint } from "../../Domain/Models/Checkpoint";
import { Order } from "../../Domain/Models/Order";
import { GetOrderResponse } from "./GetOrderResponse";
import { GetOrdersResponse } from "./GetOrdersResponse";

describe("Get Orders response Response", () => {
  it("should be created successfully", () => {
    const orderId = 1;
    const number = "orderNumber";
    const trackingNumber = "trackingNumber";
    const courier = "courier";
    const street = "street";
    const zipCode = 1212;
    const city = "city";
    const destinationCountry = "";
    const email = "email@email.com";

    const order = Order.hydrate(
      orderId,
      number,
      trackingNumber,
      courier,
      street,
      zipCode,
      city,
      destinationCountry,
      email,
    );

    const checkpointId = 2;
    const statusText = "statusText";

    const checkpoint = Checkpoint.hydrate(
      checkpointId,
      orderId,
      null,
      null,
      null,
      statusText,
      null,
    );

    const orderResponse = GetOrderResponse.create(order, checkpoint);

    const response = GetOrdersResponse.create([orderResponse]);

    const expected = {
      orders: [
        {
          order: {
            city: "city",
            number: "orderNumber",
            trackingNumber: "trackingNumber",
            street: "street",
            zipCode: 1212,
          },
          checkpoint: {
            status: statusText,
          },
        },
      ],
    };

    expect(response.toJson()).toEqual(expected);
  });
});
