import { GetOrderResponse, GetOrderResponseJson } from "./GetOrderResponse";

export interface GetOrdersResponseJson {
  orders: GetOrderResponseJson[];
}

export class GetOrdersResponse {
  private orders: GetOrderResponse[];

  private constructor(orders: GetOrderResponse[]) {
    this.orders = orders;
  }

  public static create(orders: GetOrderResponse[]): GetOrdersResponse {
    return new this(orders);
  }

  public toJson(): GetOrdersResponseJson {
    return {
      orders: this.orders.map((order) => order.toJson()),
    };
  }
}
