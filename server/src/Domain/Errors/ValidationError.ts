export class ValidationError extends Error {
  constructor(m = "validation error") {
    super(m);
  }
}
