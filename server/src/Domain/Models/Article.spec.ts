import "jest";

import {
  ArticleImageUrl,
  ArticleNumber,
  Id,
  ProductName,
  Quantity,
} from "../ValueObjects";
import { Article } from "./Article";

describe("Article", () => {
  it("Can be created", () => {
    const id = Id.create(1);
    const number = ArticleNumber.create("number");
    const imageUrl = ArticleImageUrl.create("url");
    const quantity = Quantity.create(2);
    const name = ProductName.create("name");

    const model = Article.create(id, number, imageUrl, quantity, name);

    expect(model.getId()).toEqual(id);
    expect(model.getNumber()).toEqual(number);
    expect(model.getImageUrl()).toEqual(imageUrl);
    expect(model.getQuantity()).toEqual(quantity);
    expect(model.getName()).toEqual(name);
  });

  it("Can be hydrated", () => {
    const id = 1;
    const number = "number";
    const imageUrl = "url";
    const quantity = 2;
    const name = "name";

    const model = Article.hydrate(id, number, imageUrl, quantity, name);

    expect(model.getId().getValue()).toEqual(id);
    expect(model.getNumber().getValue()).toEqual(number);
    expect(model.getImageUrl().getValue()).toEqual(imageUrl);
    expect(model.getQuantity().getValue()).toEqual(quantity);
    expect(model.getName().getValue()).toEqual(name);
  });
});
