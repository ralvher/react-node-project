import {
  Id,
  ArticleNumber,
  ArticleImageUrl,
  Quantity,
  ProductName,
} from "../ValueObjects";

export class Article {
  private id: Id;
  private number: ArticleNumber;
  private imageUrl: ArticleImageUrl;
  private quantity: Quantity;
  private name: ProductName;

  private constructor(
    id: Id,
    number: ArticleNumber,
    imageUrl: ArticleImageUrl,
    quantity: Quantity,
    name: ProductName,
  ) {
    this.id = id;
    this.number = number;
    this.imageUrl = imageUrl;
    this.quantity = quantity || null;
    this.name = name || null;
  }

  public static create(
    id: Id,
    number: ArticleNumber,
    imageUrl: ArticleImageUrl,
    quantity: Quantity,
    name: ProductName,
  ): Article {
    return new this(id, number, imageUrl, quantity, name);
  }

  public static hydrate(
    id: number,
    number: string,
    imageUrl: string,
    quantity: number,
    name: string,
  ): Article {
    return new this(
      Id.create(id),
      ArticleNumber.create(number),
      ArticleImageUrl.create(imageUrl),
      Quantity.create(quantity),
      ProductName.create(name),
    );
  }

  public getId(): Id {
    return this.id;
  }

  public getNumber(): ArticleNumber {
    return this.number;
  }
  public getImageUrl(): ArticleImageUrl {
    return this.imageUrl;
  }
  public getQuantity(): Quantity {
    return this.quantity;
  }
  public getName(): ProductName {
    return this.name;
  }
}
