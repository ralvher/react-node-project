import "jest";

import {
  Id,
  Location,
  Timestamp,
  Status,
  StatusText,
  StatusDetails,
} from "../ValueObjects";
import { Checkpoint } from "./Checkpoint";

describe("Checkpoint", () => {
  it("Can be created", () => {
    const id = Id.create(1);
    const location = Location.create("location");
    const timestap = Timestamp.create(new Date());
    const status = Status.create("status");
    const statusText = StatusText.create("statusText");
    const statusDetails = StatusDetails.create("statusDetails");
    const orderId = Id.create(2);

    const model = Checkpoint.create(
      id,
      location,
      timestap,
      status,
      statusText,
      statusDetails,
      orderId,
    );

    expect(model.getId()).toEqual(id);
    expect(model.getLocation()).toEqual(location);
    expect(model.getTimestamp()).toEqual(timestap);
    expect(model.getStatus()).toEqual(status);
    expect(model.getStatusText()).toEqual(statusText);
    expect(model.getStatusDetails()).toEqual(statusDetails);
    expect(model.getOrderId()).toEqual(orderId);
  });

  it("Can be hydrated", () => {
    const id = 1;
    const location = "location";
    const timestap = new Date();
    const status = "status";
    const statusText = "statusText";
    const statusDetails = "statusDetails";
    const orderId = 2;

    const model = Checkpoint.hydrate(
      id,
      orderId,
      location,
      timestap,
      status,
      statusText,
      statusDetails,
    );

    expect(model.getId().getValue()).toEqual(id);
    expect(model.getLocation().getValue()).toEqual(location);
    expect(model.getTimestamp()?.getValue()).toEqual(timestap);
    expect(model.getStatus().getValue()).toEqual(status);
    expect(model.getStatusText().getValue()).toEqual(statusText);
    expect(model.getStatusDetails().getValue()).toEqual(statusDetails);
    expect(model.getOrderId().getValue()).toEqual(orderId);
  });
});
