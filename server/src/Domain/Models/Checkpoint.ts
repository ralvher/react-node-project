import {
  Id,
  Location,
  Timestamp,
  Status,
  StatusText,
  StatusDetails,
} from "../ValueObjects";
export class Checkpoint {
  private id: Id;
  private location: Location;
  private timestap: Timestamp | null;
  private status: Status;
  private statusText: StatusText;
  private statusDetails: StatusDetails;
  private orderId: Id;

  private constructor(
    id: Id,
    location: Location,
    timestap: Timestamp | null,
    status: Status,
    statusText: StatusText,
    statusDetails: StatusDetails,
    orderId: Id,
  ) {
    this.id = id;
    this.location = location;
    this.timestap = timestap;
    this.status = status;
    this.statusText = statusText;
    this.statusDetails = statusDetails;
    this.orderId = orderId;
  }

  public static create(
    id: Id,
    location: Location,
    timestap: Timestamp | null,
    status: Status,
    statusText: StatusText,
    statusDetails: StatusDetails,
    orderId: Id,
  ): Checkpoint {
    return new this(
      id,
      location,
      timestap,
      status,
      statusText,
      statusDetails,
      orderId,
    );
  }

  public static hydrate(
    id: number,
    orderId: number,
    location: string | null,
    timestap: Date | null,
    status: string | null,
    statusText: string | null,
    statusDetails: string | null,
  ): Checkpoint {
    return new this(
      Id.create(id),
      Location.create(location || ""),
      timestap && Timestamp.create(timestap),
      Status.create(status || ""),
      StatusText.create(statusText || ""),
      StatusDetails.create(statusDetails || ""),
      Id.create(orderId),
    );
  }

  public getId(): Id {
    return this.id;
  }

  public getOrderId(): Id {
    return this.orderId;
  }

  public getLocation(): Location {
    return this.location;
  }

  public getTimestamp(): Timestamp | null {
    return this.timestap;
  }

  public getStatus(): Status {
    return this.status;
  }

  public getStatusText(): StatusText {
    return this.statusText;
  }

  public getStatusDetails(): StatusDetails {
    return this.statusDetails;
  }
}
