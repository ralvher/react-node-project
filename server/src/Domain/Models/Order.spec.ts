import "jest";

import {
  City,
  Courier,
  DestinationCountry,
  Email,
  Id,
  OrderNumber,
  Street,
  TrackingNumber,
  ZipCode,
} from "../ValueObjects";
import { Order } from "./Order";

describe("Order", () => {
  it("Can be created", () => {
    const id = Id.create(1);
    const trackingNumber = TrackingNumber.create("trackingNumber");
    const number = OrderNumber.create("orderNumber");
    const courier = Courier.create("courier");
    const street = Street.create("street");
    const zipCode = ZipCode.create(23244);
    const city = City.create("city");
    const destinationCountry = DestinationCountry.create("destinationCountry");
    const email = Email.create("email@email.com");

    const model = Order.create(
      id,
      number,
      trackingNumber,
      courier,
      street,
      zipCode,
      city,
      destinationCountry,
      email,
    );

    expect(model.getId()).toEqual(id);
    expect(model.getNumber()).toEqual(number);
    expect(model.getTrackingNumber()).toEqual(trackingNumber);
    expect(model.getCourier()).toEqual(courier);
    expect(model.getStreet()).toEqual(street);
    expect(model.getZipCode()).toEqual(zipCode);
    expect(model.getCity()).toEqual(city);
    expect(model.getDestionationCountry()).toEqual(destinationCountry);
    expect(model.getEmail()).toEqual(email);
  });

  it("Can be hydrated", () => {
    const id = 1;
    const trackingNumber = "trackingNumber";
    const number = "orderNumber";
    const courier = "courier";
    const street = "street";
    const zipCode = 23244;
    const city = "city";
    const destinationCountry = "destinationCountry";
    const email = "email@email.com";

    const model = Order.hydrate(
      id,
      number,
      trackingNumber,
      courier,
      street,
      zipCode,
      city,
      destinationCountry,
      email,
    );

    expect(model.getId().getValue()).toEqual(id);
    expect(model.getNumber().getValue()).toEqual(number);
    expect(model.getTrackingNumber().getValue()).toEqual(trackingNumber);
    expect(model.getCourier().getValue()).toEqual(courier);
    expect(model.getStreet().getValue()).toEqual(street);
    expect(model.getZipCode().getValue()).toEqual(zipCode);
    expect(model.getCity().getValue()).toEqual(city);
    expect(model.getDestionationCountry().getValue()).toEqual(
      destinationCountry,
    );
    expect(model.getEmail().getValue()).toEqual(email);
  });
});
