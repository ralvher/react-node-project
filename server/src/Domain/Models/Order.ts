import {
  Id,
  TrackingNumber,
  OrderNumber,
  Courier,
  Street,
  ZipCode,
  City,
  DestinationCountry,
  Email,
} from "../ValueObjects";

export class Order {
  private id: Id;
  private number: OrderNumber;
  private trackingNumber: TrackingNumber;
  private courier: Courier;
  private street: Street;
  private zipCode: ZipCode;
  private city: City;
  private destinationCountryIso3: DestinationCountry;
  private email: Email;

  private constructor(
    id: Id,
    orderNumber: OrderNumber,
    trackingNumber: TrackingNumber,
    courier: Courier,
    street: Street,
    zipCode: ZipCode,
    city: City,
    destinationCountryIso3: DestinationCountry,
    email: Email,
  ) {
    this.id = id;
    this.trackingNumber = trackingNumber;
    this.number = orderNumber;
    this.courier = courier;
    this.street = street;
    this.zipCode = zipCode;
    this.city = city;
    this.destinationCountryIso3 = destinationCountryIso3;
    this.email = email;
  }

  public static create(
    id: Id,
    number: OrderNumber,
    trackingNumber: TrackingNumber,
    courier: Courier,
    street: Street,
    zipCode: ZipCode,
    city: City,
    destinationCountryIso3: DestinationCountry,
    email: Email,
  ): Order {
    return new this(
      id,
      number,
      trackingNumber,
      courier,
      street,
      zipCode,
      city,
      destinationCountryIso3,
      email,
    );
  }

  public static hydrate(
    id: number,
    number: string,
    trackingNumber: string,
    courier: string,
    street: string,
    zipCode: number,
    city: string,
    destinationCountryIso3: string,
    email: string,
  ): Order {
    return new this(
      Id.create(id),
      OrderNumber.create(number),
      TrackingNumber.create(trackingNumber),
      Courier.create(courier),
      Street.create(street),
      ZipCode.create(zipCode),
      City.create(city),
      DestinationCountry.create(destinationCountryIso3),
      Email.create(email),
    );
  }

  public getId(): Id {
    return this.id;
  }
  public getNumber(): OrderNumber {
    return this.number;
  }
  public getTrackingNumber(): TrackingNumber {
    return this.trackingNumber;
  }
  public getCourier(): Courier {
    return this.courier;
  }
  public getStreet(): Street {
    return this.street;
  }
  public getZipCode(): ZipCode {
    return this.zipCode;
  }
  public getCity(): City {
    return this.city;
  }
  public getDestionationCountry(): DestinationCountry {
    return this.destinationCountryIso3;
  }
  public getEmail(): Email {
    return this.email;
  }
}
