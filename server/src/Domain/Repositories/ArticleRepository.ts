import { Article } from "../Models/Article";
import { OrderNumber } from "../ValueObjects";

export interface ArticleRepository {
  findByOrderNumber(orderNumber: OrderNumber): Promise<Article[]>;
}
