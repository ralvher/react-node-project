import { Checkpoint } from "../Models/Checkpoint";
import { OrderNumber } from "../ValueObjects";

export interface CheckpointRepository {
  getLastByOrderNumber(number: OrderNumber): Promise<Checkpoint | null>;
}
