import { Order } from "../Models/Order";
import { Email, OrderNumber } from "../ValueObjects";

export interface OrderRepository {
  findByEmail(email: Email): Promise<Order[]>;
  getByNumber(number: OrderNumber): Promise<Order | null>;
}
