export class ArticleImageUrl {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): ArticleImageUrl {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
