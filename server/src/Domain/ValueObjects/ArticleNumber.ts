export class ArticleNumber {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): ArticleNumber {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
