export class City {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): City {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
