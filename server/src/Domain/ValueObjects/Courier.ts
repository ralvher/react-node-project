export class Courier {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): Courier {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
