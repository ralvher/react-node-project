export class DestinationCountry {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): DestinationCountry {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
