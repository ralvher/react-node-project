import { ValidationError } from "../Errors/ValidationError";

export class Email {
  static readonly EMAIL_REGEX = /^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/;
  private value: string;

  private constructor(value: string) {
    if (!value || !value.match(Email.EMAIL_REGEX)) {
      throw new ValidationError("invalid email");
    }
    this.value = value;
  }

  public static create(value: string): Email {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
