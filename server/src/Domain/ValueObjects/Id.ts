export class Id {
  private value: number;

  private constructor(value: number) {
    this.value = value;
  }

  public static create(value: number): Id {
    return new this(value);
  }

  getValue(): number {
    return this.value;
  }
}
