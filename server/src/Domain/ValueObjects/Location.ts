export class Location {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): Location {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
