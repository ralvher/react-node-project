export class OrderNumber {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): OrderNumber {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
