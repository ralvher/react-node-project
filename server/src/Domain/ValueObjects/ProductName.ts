export class ProductName {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): ProductName {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
