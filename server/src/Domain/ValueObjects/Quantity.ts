export class Quantity {
  private value: number;

  private constructor(value: number) {
    this.value = value;
  }

  public static create(value: number): Quantity {
    return new this(value);
  }

  getValue(): number {
    return this.value;
  }
}
