export class Status {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): Status {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
