export class StatusDetails {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): StatusDetails {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
