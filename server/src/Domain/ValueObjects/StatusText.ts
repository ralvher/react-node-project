export class StatusText {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): StatusText {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
