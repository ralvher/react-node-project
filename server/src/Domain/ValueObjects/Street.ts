export class Street {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): Street {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
