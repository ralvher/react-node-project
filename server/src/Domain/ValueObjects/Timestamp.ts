export class Timestamp {
  private value: Date;

  private constructor(value: Date) {
    this.value = new Date(value);
  }

  public static create(value: Date): Timestamp {
    return new this(value);
  }

  getValue(): Date {
    return this.value;
  }
}
