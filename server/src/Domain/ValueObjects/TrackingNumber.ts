export class TrackingNumber {
  private value: string;

  private constructor(value: string) {
    this.value = value;
  }

  public static create(value: string): TrackingNumber {
    return new this(value);
  }

  getValue(): string {
    return this.value;
  }
}
