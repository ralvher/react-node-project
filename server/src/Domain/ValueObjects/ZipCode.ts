export class ZipCode {
  private value: number;

  private constructor(value: number) {
    this.value = value;
  }

  public static create(value: number): ZipCode {
    return new this(value);
  }

  getValue(): number {
    return this.value;
  }
}
