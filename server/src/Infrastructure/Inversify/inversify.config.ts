import { Container } from "inversify";
import { PrismaClient } from "@prisma/client";
import { OrderRepository } from "../../Domain/Repositories/OrderRepository";
import { CheckpointRepository } from "../../Domain/Repositories/CheckpointRepository";
import { PrismaCheckpointRepository } from "../Repositories/PrismaCheckpointRepository";
import { PrismaOrderRepository } from "../Repositories/PrismaOrderRepository";
import { ArticleRepository } from "../../Domain/Repositories/ArticleRepository";
import { PrismaArticleRepository } from "../Repositories/PrismaArticleRepository";
import { GetOrdersHandler } from "../../Application/Handlers/GetOrdersHandler";
import { GetOrderInformationHandler } from "../../Application/Handlers/GetOrderInformationHandler";

const container = new Container();

//External
container
  .bind<PrismaClient>("PrismaClient")
  .toConstantValue(new PrismaClient());

//Handlers
container.bind<GetOrdersHandler>(GetOrdersHandler).toSelf();
container.bind<GetOrderInformationHandler>(GetOrderInformationHandler).toSelf();

//Repositories
container.bind<OrderRepository>("OrderRepository").to(PrismaOrderRepository);
container
  .bind<CheckpointRepository>("CheckpointRepository")
  .to(PrismaCheckpointRepository);
container
  .bind<ArticleRepository>("ArticleRepository")
  .to(PrismaArticleRepository);

export { container };
