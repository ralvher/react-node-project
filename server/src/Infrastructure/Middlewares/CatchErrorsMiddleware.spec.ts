import "jest";

import { Request, Response } from "express";
import CatchErrorsMiddleware from "./CatchErrorsMiddleware";
import { NotFoundError } from "../../Application/Errors/NotFoundError";
import { ValidationError } from "../../Domain/Errors/ValidationError";

describe("Catch Errors Middleware", () => {
  it("returns next when headers already sent", () => {
    const response: Partial<Response> = {
      headersSent: true,
    };
    const nextFunction = () => {
      return "ok";
    };
    const request: Partial<Request> = {};

    const middleware = CatchErrorsMiddleware(
      new Error(),
      request as Request,
      response as Response,
      nextFunction,
    );
    expect(middleware).toEqual("ok");
  });

  it("returns 404 when NotFound Error", () => {
    const response: Partial<Response> = {
      headersSent: false,
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    const request: Partial<Request> = {};

    CatchErrorsMiddleware(
      new NotFoundError(),
      request as Request,
      response as Response,
      jest.fn(),
    );
    expect(response.status).toHaveBeenCalledWith(404);
    expect(response.json).toHaveBeenCalledWith({
      message: "not found",
    });
  });

  it("returns 422 when Validation Error", () => {
    const response: Partial<Response> = {
      headersSent: false,
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    const request: Partial<Request> = {};

    CatchErrorsMiddleware(
      new ValidationError(),
      request as Request,
      response as Response,
      jest.fn(),
    );
    expect(response.status).toHaveBeenCalledWith(422);
    expect(response.json).toHaveBeenCalledWith({
      message: "validation error",
    });
  });

  it("returns 500 when Error", () => {
    const response: Partial<Response> = {
      headersSent: false,
      status: jest.fn().mockReturnThis(),
      json: jest.fn(),
    };
    const request: Partial<Request> = {};

    CatchErrorsMiddleware(
      new Error(),
      request as Request,
      response as Response,
      jest.fn(),
    );
    expect(response.status).toHaveBeenCalledWith(500);
    expect(response.json).toHaveBeenCalledWith({ message: "unknown error" });
  });
});
