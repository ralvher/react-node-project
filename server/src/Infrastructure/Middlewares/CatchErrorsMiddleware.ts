import { Request, Response, NextFunction } from "express";
import { NotFoundError } from "../../Application/Errors/NotFoundError";
import { ValidationError } from "../../Domain/Errors/ValidationError";

export default function (
  err: Error,
  req: Request,
  res: Response,
  next: NextFunction,
): unknown {
  if (res.headersSent) {
    return next(err);
  }

  switch (true) {
    case err instanceof NotFoundError:
      return res.status(404).json({
        message: err.message,
      });
    case err instanceof ValidationError:
      return res.status(422).json({
        message: err.message,
      });
    default:
      return res.status(500).json({
        message: "unknown error",
      });
  }
}
