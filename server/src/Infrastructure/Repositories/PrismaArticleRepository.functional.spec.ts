import "jest";

import { PrismaClient } from "@prisma/client";
import { PrismaArticleRepository } from "./PrismaArticleRepository";
import { OrderNumber } from "../../Domain/ValueObjects";

describe("Prisma Article Repository", () => {
  const db = new PrismaClient();

  const repository = new PrismaArticleRepository(db);

  beforeAll(async () => {
    const order = await db.order.create({
      data: {
        city: "city",
        courier: "courier",
        destinationCountryIso3: "destinationCountry",
        email: "test@test.com",
        number: "orderNumber",
        trackingNumber: "trackingNumber",
        street: "street",
        zipCode: 12345,
      },
    });

    await db.article.create({
      data: {
        name: "name",
        orderId: order.id,
        imageUrl: "imageUrl",
        number: "number",
        quantity: 2,
      },
    });
  });

  afterAll(() => {
    db.$disconnect();
  });

  it("find some tracking articles", async () => {
    const stored = await repository.findByOrderNumber(
      OrderNumber.create("orderNumber"),
    );

    expect(stored.length).toEqual(1);
  });
  it("not find any tracking articles", async () => {
    const stored = await repository.findByOrderNumber(
      OrderNumber.create("1234"),
    );

    expect(stored.length).toEqual(0);
  });
});
