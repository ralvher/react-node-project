import "reflect-metadata";
import { inject, injectable } from "inversify";
import { PrismaClient } from ".prisma/client";

import { Article } from "../../Domain/Models/Article";
import { ArticleRepository } from "../../Domain/Repositories/ArticleRepository";
import { OrderNumber } from "../../Domain/ValueObjects";

@injectable()
export class PrismaArticleRepository implements ArticleRepository {
  private db: PrismaClient;

  constructor(@inject("PrismaClient") db: PrismaClient) {
    this.db = db;
  }

  async findByOrderNumber(number: OrderNumber): Promise<Article[]> {
    const models = await this.db.article.findMany({
      where: {
        order: {
          number: number.getValue(),
        },
      },
    });

    return models.map((model) =>
      Article.hydrate(
        model.id,
        model.number,
        model.imageUrl,
        model.quantity,
        model.name,
      ),
    );
  }
}
