import "jest";

import { PrismaClient } from "@prisma/client";
import { OrderNumber } from "../../Domain/ValueObjects";
import { PrismaCheckpointRepository } from "./PrismaCheckpointRepository";

describe("Prisma Checkpoint Repository", () => {
  const db = new PrismaClient();

  const repository = new PrismaCheckpointRepository(db);

  beforeAll(async () => {
    const order = await db.order.create({
      data: {
        city: "city",
        courier: "courier",
        destinationCountryIso3: "destinationCountry",
        email: "test@test.com",
        number: "orderNumber",
        trackingNumber: "trackingNumber",
        street: "street",
        zipCode: 12345,
      },
    });

    await db.checkpoint.create({
      data: {
        location: "location",
        statusText: "statusText",
        orderId: order.id,
      },
    });
  });

  afterAll(() => {
    db.$disconnect();
  });

  it("find some order checkpoints", async () => {
    const stored = await repository.getLastByOrderNumber(
      OrderNumber.create("orderNumber"),
    );

    expect(stored).toBeDefined();
    expect(stored?.getStatusText().getValue()).toEqual("statusText");
  });
  it("not find any order checkpoint", async () => {
    const stored = await repository.getLastByOrderNumber(
      OrderNumber.create("orderNumber1"),
    );

    expect(stored).toBeNull();
  });
});
