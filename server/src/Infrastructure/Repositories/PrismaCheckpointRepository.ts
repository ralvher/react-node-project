import "reflect-metadata";
import { inject, injectable } from "inversify";
import { PrismaClient } from ".prisma/client";

import { OrderNumber } from "../../Domain/ValueObjects";
import { CheckpointRepository } from "../../Domain/Repositories/CheckpointRepository";
import { Checkpoint } from "../../Domain/Models/Checkpoint";

@injectable()
export class PrismaCheckpointRepository implements CheckpointRepository {
  private db: PrismaClient;

  constructor(@inject("PrismaClient") db: PrismaClient) {
    this.db = db;
  }

  async getLastByOrderNumber(number: OrderNumber): Promise<Checkpoint | null> {
    const model = await this.db.checkpoint.findFirst({
      where: {
        order: {
          number: number.getValue(),
        },
      },
      orderBy: {
        timestamp: "desc",
      },
    });

    if (!model) {
      return null;
    }

    return Checkpoint.hydrate(
      model.id,
      model.orderId,
      model.location,
      model.timestamp,
      model.status,
      model.statusText,
      model.statusDetails,
    );
  }
}
