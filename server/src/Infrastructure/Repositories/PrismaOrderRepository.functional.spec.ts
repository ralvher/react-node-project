import "jest";

import { PrismaClient } from "@prisma/client";
import { PrismaOrderRepository } from "./PrismaOrderRepository";
import { Email } from "../../Domain/ValueObjects";

describe("Prisma Order Repository", () => {
  const db = new PrismaClient();

  const repository = new PrismaOrderRepository(db);

  beforeAll(async () => {
    await db.order.create({
      data: {
        city: "city",
        courier: "courier",
        destinationCountryIso3: "destinationCountry",
        email: "test@test.com",
        number: "orderNumber",
        trackingNumber: "trackingNumber",
        street: "street",
        zipCode: 12345,
      },
    });
  });

  afterAll(() => {
    db.$disconnect();
  });

  it("find return orders when email is in the database", async () => {
    const stored = await repository.findByEmail(Email.create("test@test.com"));

    expect(stored.length).toEqual(1);
  });

  it("find return empty order when email is not in the database", async () => {
    const stored = await repository.findByEmail(
      Email.create("testando@test.com"),
    );

    expect(stored.length).toEqual(0);
  });
});
