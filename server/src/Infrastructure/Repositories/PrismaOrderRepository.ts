import "reflect-metadata";
import { inject, injectable } from "inversify";
import { PrismaClient } from ".prisma/client";

import { Order } from "../../Domain/Models/Order";
import { OrderRepository } from "../../Domain/Repositories/OrderRepository";
import { Email, OrderNumber } from "../../Domain/ValueObjects";

@injectable()
export class PrismaOrderRepository implements OrderRepository {
  private db: PrismaClient;

  constructor(@inject("PrismaClient") db: PrismaClient) {
    this.db = db;
  }

  async findByEmail(email: Email): Promise<Order[]> {
    const models = await this.db.order.findMany({
      where: {
        email: email.getValue(),
      },
    });

    return models.map((model) =>
      Order.hydrate(
        model.id,
        model.number,
        model.trackingNumber,
        model.courier,
        model.street,
        model.zipCode,
        model.city,
        model.destinationCountryIso3,
        model.email,
      ),
    );
  }

  async getByNumber(number: OrderNumber): Promise<Order | null> {
    const model = await this.db.order.findUnique({
      where: {
        number: number.getValue(),
      },
    });

    if (!model) {
      return null;
    }

    return Order.hydrate(
      model.id,
      model.number,
      model.trackingNumber,
      model.courier,
      model.street,
      model.zipCode,
      model.city,
      model.destinationCountryIso3,
      model.email,
    );
  }
}
