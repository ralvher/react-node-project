import { NextFunction, Request, Response } from "express";
type AppController = (request: Request, response: Response) => Promise<void>;

const ErrorControllerWrapper = (controller: AppController) => async (
  request: Request,
  response: Response,
  next: NextFunction,
): Promise<void> => {
  try {
    await controller(request, response);
  } catch (e) {
    next(e);
  }
};

export default ErrorControllerWrapper;
