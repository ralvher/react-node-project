import "jest";
import request from "supertest";
import { PrismaClient } from "@prisma/client";
import { server } from "../../../server";

const db = new PrismaClient();

beforeAll(async () => {
  const order = await db.order.create({
    data: {
      city: "city",
      courier: "courier",
      destinationCountryIso3: "destinationCountry",
      email: "test@test.com",
      number: "orderNumber",
      trackingNumber: "trackingNumber",
      street: "street",
      zipCode: 12345,
    },
  });

  await db.checkpoint.create({
    data: {
      orderId: order.id,
      statusDetails: "statusDetails",
      statusText: "Delivered",
    },
  });

  await db.article.create({
    data: {
      orderId: order.id,
      name: "article",
      imageUrl: "imageUrl",
      quantity: 1,
      number: "articleNumber",
    },
  });
});

afterAll(() => {
  db.$disconnect();
});

test("a user can join to meeting", async () => {
  await request(server)
    .get(`/orders/orderNumber`)
    .set("Accept-Encoding", "application/json")
    .expect(200)
    .expect("Content-Type", /application\/json/)
    .then((response) =>
      expect(response.body).toEqual({
        articles: [
          {
            image: "imageUrl",
            name: "article",
            number: "articleNumber",
            quantity: 1,
          },
        ],
        checkpoint: {
          details: "statusDetails",
          status: "Delivered",
        },
        city: "city",
        number: "orderNumber",
        trackingNumber: "trackingNumber",
        street: "street",
        zipCode: 12345,
      }),
    );
});

test("a 404 is thrown when order does not exist", async () => {
  await request(server).get(`/orders/invalid-order-number`).expect(404);
});
