import { Request, Response } from "express";

import ErrorControllerWrapper from "./ErrorControllerWrapper";
import container from "../../../Infrastructure/Inversify";
import { GetOrderInformationQuery } from "../../../Application/Queries/GetOrderInformationQuery";
import { GetOrderInformationHandler } from "../../../Application/Handlers/GetOrderInformationHandler";
import GetOrderInformationTransformer from "../Transformers/GetOrderInformationTransformer";

async function GetOrderInformationController(
  request: Request,
  response: Response,
) {
  const getOrderInformationHandler = container.get<GetOrderInformationHandler>(
    GetOrderInformationHandler,
  );

  const orderNumber = request.params.number;

  const result = await getOrderInformationHandler.handle(
    GetOrderInformationQuery.create(orderNumber),
  );

  response.json(GetOrderInformationTransformer(result.toJson()));
}

export default ErrorControllerWrapper(GetOrderInformationController);
