import "jest";
import request from "supertest";
import { PrismaClient } from "@prisma/client";
import { server } from "../../../server";

const db = new PrismaClient();

beforeAll(async () => {
  const order = await db.order.create({
    data: {
      city: "city",
      courier: "courier",
      destinationCountryIso3: "destinationCountry",
      email: "test@test.com",
      number: "orderNumber",
      trackingNumber: "trackingNumber",
      street: "street",
      zipCode: 12345,
    },
  });

  await db.checkpoint.create({
    data: {
      orderId: order.id,
      statusText: "Delivered",
    },
  });
});

afterAll(() => {
  db.$disconnect();
});

test("a user can join to meeting", async () => {
  await request(server)
    .post(`/orders`)
    .set("Accept-Encoding", "application/json")
    .send({ email: "test@test.com" })
    .expect(200)
    .expect("Content-Type", /application\/json/)
    .then((response) =>
      expect(response.body).toEqual([
        {
          checkpointStatus: "Delivered",
          city: "city",
          number: "orderNumber",
          trackingNumber: "trackingNumber",
          street: "street",
          zipCode: 12345,
        },
      ]),
    );
});

test("a 422 is thrown when email is  invalid", async () => {
  const invalidEmail = "invalidEmail";
  await request(server)
    .post(`/orders`)
    .send({ email: invalidEmail })
    .expect(422);
});
