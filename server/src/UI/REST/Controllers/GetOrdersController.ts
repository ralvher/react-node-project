import { Request, Response } from "express";

import ErrorControllerWrapper from "./ErrorControllerWrapper";
import container from "../../../Infrastructure/Inversify";
import { GetOrdersQuery } from "../../../Application/Queries/GetOrdersQuery";
import { GetOrdersHandler } from "../../../Application/Handlers/GetOrdersHandler";
import GetOrdersTransformer from "../Transformers/GetOrdersTransformer";

async function GetOrdersController(request: Request, response: Response) {
  const getOrdersHandler = container.get<GetOrdersHandler>(GetOrdersHandler);

  const email = String(request.query.email);

  const result = await getOrdersHandler.handle(GetOrdersQuery.create(email));

  response.json(GetOrdersTransformer(result.toJson()));
}

export default ErrorControllerWrapper(GetOrdersController);
