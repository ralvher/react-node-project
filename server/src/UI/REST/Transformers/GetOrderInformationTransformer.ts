import { GetOrderInformationResponseJson } from "../../../Application/Responses/GetOrderInformationResponse";

interface CheckpointTransformer {
  status: string;
  details: string;
}

interface ArticlesTransformer {
  image: string;
  name: string;
  number: string;
  quantity: number;
}

interface OrderInformationTransformer {
  number: string;
  trackingNumber: string;
  street: string;
  zipCode: number;
  city: string;
  checkpoint: CheckpointTransformer;
  articles: ArticlesTransformer[];
}

export default function GetOrderInformationTransformer(
  data: GetOrderInformationResponseJson,
): OrderInformationTransformer {
  return {
    number: data.order.number,
    trackingNumber: data.order.trackingNumber,
    street: data.order.street,
    zipCode: data.order.zipCode,
    city: data.order.city,
    checkpoint: {
      status: data.checkpoint.status,
      details: data.checkpoint.details,
    },
    articles: data.articles.map((article) => ({
      image: article.image,
      name: article.name,
      number: article.number,
      quantity: article.quantity,
    })),
  };
}
