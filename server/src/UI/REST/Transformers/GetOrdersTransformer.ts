import { GetOrdersResponseJson } from "../../../Application/Responses/GetOrdersResponse";

interface OrdersTransformer {
  number: string;
  trackingNumber: string;
  street: string;
  zipCode: number;
  city: string;
  checkpointStatus: string;
}

export default function GetOrdersTransformer(
  data: GetOrdersResponseJson,
): OrdersTransformer[] {
  return data.orders.map((order) => ({
    number: order.order.number,
    trackingNumber: order.order.trackingNumber,
    street: order.order.street,
    zipCode: order.order.zipCode,
    city: order.order.city,
    checkpointStatus: order.checkpoint.status,
  }));
}
