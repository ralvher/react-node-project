import { server } from "./server";

const { PORT = 80 } = process.env;

server.listen(PORT, () => console.log(`server listening in ${PORT}`));
