import { parse } from "papaparse";
import { readFileSync } from "fs";
import { PrismaClient } from ".prisma/client";

const prisma = new PrismaClient();

const files = ["trackings.csv", "checkpoints.csv"];

async function seedTrackings(fileName: string) {
  return new Promise((resolve, reject) => {
    const file = readFileSync(`${__dirname}/resources/${fileName}`, "utf8");

    parse(file, {
      skipEmptyLines: true,
      dynamicTyping: true,
      error: reject,
      complete: async (result) => {
        const rows = result.data.slice(1);
        for await (const element of rows) {
          const [
            number,
            trackingNumber,
            courier,
            street,
            zipCode,
            city,
            destinationCountryIso3,
            email,
            articleNum,
            imageUrl,
            quantity,
            name,
          ] = element as unknown[];

          const order = await prisma.order.upsert({
            where: {
              number: String(number),
            },
            update: {},
            create: {
              number: String(number),
              trackingNumber: String(trackingNumber),
              courier: String(courier),
              street: String(street),
              zipCode: Number(zipCode),
              city: String(city),
              destinationCountryIso3: String(destinationCountryIso3),
              email: String(email),
            },
          });

          if (articleNum) {
            await prisma.article.upsert({
              where: {
                number: String(articleNum),
              },
              update: {},
              create: {
                number: String(articleNum),
                imageUrl: String(imageUrl),
                quantity: Number(quantity),
                name: String(name),
                orderId: order.id,
              },
            });
          }
        }
        resolve("done");
      },
    });
  });
}

async function seedCheckpoints(fileName: string) {
  return new Promise((resolve, reject) => {
    const file = readFileSync(`${__dirname}/resources/${fileName}`, "utf8");

    parse(file, {
      skipEmptyLines: true,
      dynamicTyping: true,
      error: reject,
      complete: async (result) => {
        const rows = result.data;

        for await (const element of rows) {
          const [
            trackingNumber,
            location,
            timestamp,
            status,
            statusText,
            statusDetails,
          ] = element as unknown[];

          const order = await prisma.order.findFirst({
            where: { trackingNumber: String(trackingNumber) },
          });

          if (order) {
            await prisma.checkpoint.create({
              data: {
                location: String(location),
                timestamp: new Date(timestamp as string),
                status: String(status),
                statusText: String(statusText),
                statusDetails: String(statusDetails),
                orderId: order.id,
              },
            });
          }
        }
        resolve("done");
      },
    });
  });
}

Promise.all([seedTrackings(files[0]), seedCheckpoints(files[1])])
  .then(() => process.exit())
  .catch((e) => {
    console.log(e);
    console.log("something happened");
  });
