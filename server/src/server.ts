import express from "express";
import cors from "cors";

import CatchErrorsMiddleware from "./Infrastructure/Middlewares/CatchErrorsMiddleware";
import GetOrdersController from "./UI/REST/Controllers/GetOrdersController";
import GetOrderInformationController from "./UI/REST/Controllers/GetOrderInformationController";

export const server = express();

const corsOptions = {
  origin: "http://localhost:3000",
};

server.use(cors(corsOptions));
server.use(express.json());

server.get("/orders", GetOrdersController);
server.get("/orders/:number", GetOrderInformationController);

server.use(CatchErrorsMiddleware);
