const execSync = require("child_process").execSync;
const NodeEnvironment = require("jest-environment-node");
const prismaBinary = "./node_modules/.bin/prisma2";

class ConfigTestEnvironment extends NodeEnvironment {
  constructor(config, context) {
    super(config, context);

    this.currentTestPath = context.testPath;
    this.connectionString = this.testingDatabasePerWorker();
  }

  async setup() {
    await super.setup();

    if (this.currentTestPath.includes(".functional.spec.")) {
      process.env.DATABASE_URL = this.connectionString;
      this.global.process.env.DATABASE_URL = this.connectionString;

      execSync(`${prismaBinary} migrate reset --force `);
    }
  }

  async teardown() {
    await super.teardown();
  }

  testingDatabasePerWorker() {
    const url = "file:database";
    const schema = `test_${process.env.JEST_WORKER_ID}.db`;

    return `${url}/${schema}`;
  }
}

module.exports = ConfigTestEnvironment;
